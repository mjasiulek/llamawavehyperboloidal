#include <assert.h>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

int
LWH_startup (void)
{
  int ierr;
  
  ierr = CCTK_RegisterBanner ("Llama Wave Hyperboloidal");
  assert (! ierr);
  
  return 0;
}

void
LWH_register_MoL (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int evolved_group, rhs_group;
  int ierr;
  
  evolved_group = CCTK_GroupIndex ("LlamaWaveHyperboloidal::scalars");
  assert (evolved_group >= 0);
  rhs_group = CCTK_GroupIndex ("LlamaWaveHyperboloidal::dt_scalars");
  assert (rhs_group >= 0);

  ierr = MoLRegisterEvolvedGroup (evolved_group, rhs_group);
  assert (! ierr);
}

